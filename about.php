<?php
error_reporting(0);
require_once 'classes/my1Class.php';
$moc = new my1class;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>My1class - <?php echo $moc->name; ?></title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="1 PHP class or function a day at your fingers." />
    <meta name="keywords" content="PHP, class, function, php, random, programming" />
    <meta name="author" content="denbatte" />
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link href="http://cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Josefin+Slab:400,700' rel='stylesheet' type='text/css' />

    <noscript>
        <style>
            .st-accordion ul li{
                height:auto;
            }
            .st-accordion ul li > a span{
                visibility:hidden;
            }
        </style>
    </noscript>

</head>
<body>
<div class="container">
    <object class="art" data="images/footer.svg" type="image/svg+xml" width="100%" height="100">
        <div></div><!-- Fallback for http://caniuse.com/#search=SVG & http://dbushell.com/2013/02/04/a-primer-to-front-end-svg-hacking/ -->
    </object>
    <h1>About this website</h1>
    <div class="wrapper">
        <div id="st-accordion" class="st-accordion">
            <ul>
                <li>
                    <a href="#">Why does this site exists?<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>
                            One day, at the social drink after a <a href="http://www.phpbenelux.com" target="_blank" >PHP-BeNeLux UG</a>
                            meeting, someone mentioned he learned <span style="text-decoration: underline;">one</span>
                            PHP function or class a day.<br />
                            The only problem the developer faced was finding a different and suitable function every day.<br />
                            We took up the challenge and created this site to automatically deliver <span style="text-decoration: underline;">one</span>
                            PHP <span style="text-decoration: underline;">class or function</span> a day.<br />
                            We made the website opensource in a bitbucket public repository so everyone can enjoy it.<br /><br />
                            <a href="https://twitter.com/my1class" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @my1class</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            We have a twitter stream with the daily function or class<br />
                            or you can subscribe to our newsletter below.
                        </p>
                    </div>
                </li>
                <li>
                    <a href="#">Copyright<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>
                            All function and class data is retrieved from the <a href="http://www.php.net" target="_blank" >php.net</a> website.<br />
                            Any of the data copyrights are therefore owned by <a href="http://www.php.net" target="_blank" >php.net</a>
                        </p>
                    </div>
                </li>
                <li>
                    <a href="#">Privacy<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>
                            We will never sell or distribute your email adress or twitter to anyone else. Its only purpose is to send
                            a daily mail with a fresh function or class.
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="footer-art">
        <object class="art" data="images/footer.svg" type="image/svg+xml" width="100%" height="100">
            <div></div>
        </object>
    </div>
    <div class="footer">
        Get a daily class or function in your mailbox.
        <div class="small-txt">My1class hates spam and will never sell or use your email address for anything else than this.</div>

        <!-- Begin MailChimp Signup Form -->
        <style type="text/css">
            #mc_embed_signup{ clear:left; }
                /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
        </style>
        <div id="mc_embed_signup">
            <form action="http://my1class.us7.list-manage.com/subscribe/post?u=5769cc9acc2a882deb24a9603&amp;id=83b33f3024" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <label for="mce-EMAIL">Subscribe to our mailing list</label>
                <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
            </form>
        </div>

        <!--End mc_embed_signup-->
        <div class="contact"><a href="https://twitter.com/my1class">Contact: @my1class</a> | <a href="index.php">Take me back home</a></div>
    </div>

</div>
<script src="//code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="js/jquery.accordion.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.min.js"></script>
<script type="text/javascript">
    $(function() {

        $('#st-accordion').accordion({
            // Er kan maar 1 item open zijn.
            oneOpenedItem	: true,
            // Item met indexnummer 1 is standaard open.
            open            : 0
        });

    });
</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-20137469-5', 'my1class.net');
    ga('send', 'pageview');

</script>
</body>
</html>
