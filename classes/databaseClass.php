<?php
require_once 'dbconfig.php';

class DatabaseClass
{

    public $connection;


    public function open_connection()
    {
//            $this->connection = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_NAME, DB_USER, DB_PASS,
//            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
//
//            if (!$this->connection) {
//                die("<br />Database connection failed: " . mysql_error());
//            } else {
//            //    var_dump($this->connection);
//            }

        try {

            $this->connection = new PDO ("sqlsrv:server = tcp:liu9g0zdig.database.windows.net,1433; Database = my1class.dbo", getenv("dbAccount"), getenv("dbAccountPassword"));
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            print("Error connecting to SQL Server.");
        }

        die(print_r($e));
    }

    public function close_connection()
    {
        if (isset($this->connection)) {
            $this->connection = null;
        }
    }

    /**
     *
     * Returns all rows from a given table
     *
     * @param String $table
     * @return Array
     */
    public function pullAll($table)
    {
        $this->open_connection();
        $sql = "SELECT * FROM $table";
        $stmt = $this->connection->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     *
     * Returns a random row from the given table.
     *
     * @param String $table
     * @return Array
     */
    public function pullRandom($table)
    {
        $this->open_connection();
        $sql = "SELECT * FROM $table ORDER BY RAND() LIMIT 1";
        $stmt = $this->connection->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->close_connection();
        $result = $result[0];
        return $result;
    }

    public function pullDay($date)
    {
        $this->open_connection();
        $sql = "SELECT * FROM `today` WHERE `datum`=$date";
        $stmt = $this->connection->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $result = json_decode($result[0]['naam']);
        $this->close_connection();
        return $result;
    }

    public function pushToday($datum, $naam)
    {
        $this->open_connection();
        $sql = "INSERT INTO `today` (datum,naam) VALUES (:datum,:naam)";
        $q = $this->connection->prepare($sql);
        $q->execute(array(':datum' => $datum,
            ':naam' => $naam));
        $this->close_connection();
    }

}
