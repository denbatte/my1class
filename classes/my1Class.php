<?php
require_once "databaseClass.php";

class My1Class extends DatabaseClass {

    public $data;
    public $name;
    public $url;
    public $date;
    public $type;
    public $version;
    public $description;
    public $use;
    public $use2;
    public $parameters;

    public function __construct() {
        $this->date = date("dmY");
        // Haal alle gegevens uit de "php" tabel op basis van de vandaag gekozen functie.
        $gegevens = $this->pullDay($this->date);
        $this->name = $gegevens->naam;
        $this->type = $gegevens->type;
        $this->url = $gegevens->url;
        // Maak 1 call naar de php.net website en sla alle gegevens op in $data -> Filter
        $this->getAllDataFromPhpdotnet();
        $this->filterVersion();
        $this->filterDescription();
        $this->filterUse();
        $this->filteruse2();
        $this->filterParameters();
    }

    public function getAllDataFromPhpdotnet() {
        $this->data = file_get_contents($this->url);
    }

    public function filterVersion() {
        $regex = '/<p class="verinfo">(.*?)<\/p>/s';
        preg_match_all($regex, $this->data, $divs);
        $this->version = $divs[1][0];
        if(substr($this->version, 0, 1) == "(") {
            $this->version;
        } elseif($this->version == NULL) {
            $this->version = "Could not retrieve version information.";
        } else {
            $this->version = "(No version information available)";
        }
    }

    public function filterDescription() {
        $regex = '/<span class="dc-title">(.*?)<\/span>/s';
        preg_match_all($regex, $this->data, $divs);
        $divs = $divs[1][0];
        $divs = str_replace("<a href=\"", "<a target=\"_blank\" href=\"http://www.php.net/manual/en/", $divs);
        $this->description = $divs;
    }

    public function filterUse() {
        $regex = '/<div class="methodsynopsis dc-description">(.*?)<\/div>/s';
        preg_match_all($regex, $this->data, $divs);
        $divs = $divs[1][0];
        $divs = str_replace("<a href=\"", "<a target=\"_blank\" href=\"http://www.php.net/manual/en/", $divs);
        $this->use = $divs;
    }

    public function filterUse2() {
        $regex = '/<p class="para rdfs-comment">(.*?)<\/p>/s';
        preg_match_all($regex, $this->data, $divs);
        $divs = $divs[1][0];
        $divs = str_replace("<a href=\"", "<a target=\"_blank\" href=\"http://www.php.net/manual/en/", $divs);
        if($divs == "") {
            $this->use2 = "No description found.";
        } else {
            $this->use2 = $divs;
        }

    }

    public function filterParameters() {
        $regex = '/<dl>(.*?)<\/dl>/s';
        preg_match_all($regex, $this->data, $divs);
        $divs = $divs[1][0];
        // Ow yeah, make sure internal links go tp the php.net site
        $divs = str_replace("<a href=\"", "<a target=\"_blank\" href=\"http://www.php.net/manual/en/", $divs);

        if($divs == "") {
            $this->parameters = "No parameter info available. <br /> For more information, take a look at the quick " . $this->type . " links section below.";
        } else {
            $this->parameters = $divs;
        }
    }

}
