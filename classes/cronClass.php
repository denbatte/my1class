<?php
require_once "databaseClass.php";
require_once "twitterClass.php";
require_once "my1Class.php";

class CronClass extends My1Class
{

    public $datum;
    public $functie;

    public function __construct()
    {

        // Haal een willekeurige functie uit de DB
        $this->functie = $this->pullRandom("php");
        // Zet ook even de huidige datum vast in een bruikbaar formaat (8).
        $this->datum = date("dmY");

        // Maak van de geselecteerde functie een json-string en zet deze in de today tabel.
        $functie = json_encode($this->functie);
        $this->pushToday($this->datum, $functie);
        $this->close_connection();

        // Post de functie van vandaag op twitter
        $moc = new My1Class;
        $this->Twitter($moc->name, $moc->type);
    }


    public function Twitter($name, $type)
    {

        // Get the info from windows Azure App settings
        $conkey = $_ENV["twitterConnectionKey"];
        $consec = $_ENV["twitterConnectionSecurity"];
        $acct = $_ENV["twitterAccountToken"];
        $acctsec = $_ENV["twitterAccountTokenSecurity"];

        $twitter = new TwitterClass($conkey, $consec, $acct, $acctsec);
        $twitter->send("Today's " . $type . ": " . $name . "\n How does it work? http://www.my1class.net");


    }

    /**
     * @TODO: Maak een functie die de mails via MCAPI automatisch verzend.
     */

}

// Voer deze classe uit als het bestand geladen wordt.
$cron = new CronClass();
